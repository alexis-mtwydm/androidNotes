package com.example.notes.data.model

data class Note (
    val id:String="",
    val title:String="",
    val comment:String="",
    val image:String=""
)
